package erosion;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

/**
 *
 * @author MultiTool
 */
public class Grid {
  public ArrayList<Cell> Cells;// alternate way
  public int NumCols, NumRows;
  public Random rand = new Random();
  /* ********************************************************************************* */
  public Grid() {
//    this.Init(3, 3);
    this.Init(10, 10);
//    this.Init(20, 20);
//    this.Init(100, 100);
  }
  /* ********************************************************************************* */
  public void Init(int NumCols, int NumRows) {
    this.NumCols = NumCols;
    this.NumRows = NumRows;
    Cell seed = new Cell();
    Cell cell;

    int NumCells = NumCols * NumRows;
    this.Cells = new ArrayList<Cell>();
    for (int CellCnt = 0; CellCnt < NumCells; CellCnt++) {
      cell = seed.Clone_Me();
      cell.MyIndex = CellCnt;
      this.Cells.add(cell);
    }
    this.Torus_Connect();
    this.Initialize_Ramp();
  }
  /* ********************************************************************************* */
  public int GetWidth() {
    return this.NumCols;
  }
  /* ********************************************************************************* */
  public void Torus_Connect() {// Connect all cells with neighbors
    int LastRow = this.NumRows - 1;
    int RowNumPrev = LastRow, RowNum = 0;
    for (int ycnt = 0; ycnt < this.NumRows; ycnt++) {
      this.ConnectCellsHoriz(RowNum);
      this.ConnectRowsVert(RowNumPrev, RowNum);
      RowNumPrev = RowNum;
      RowNum++;
    }
  }
  /* ********************************************************************************* */
  public void ConnectRowsVert(int RowNum0, int RowNum1) {// connect one row to another
    int Dex0 = RowNum0 * this.NumCols;
    int Dex1 = RowNum1 * this.NumCols;
    for (int cnt = 0; cnt < NumCols; cnt++) {
      Cell cell0 = this.Cells.get(Dex0);
      Cell cell1 = this.Cells.get(Dex1);
      cell1.CrossConnect(cell0, 1, 0);// 1 is ydim, 0 is north pole
      Dex0++;
      Dex1++;
    }
  }
  /* ********************************************************************************* */
  public void ConnectCellsHoriz(int RowNum) {// wrapped horizontal connections
    int XDex0 = RowNum * this.NumCols;
    int XDex1 = XDex0 + this.NumCols;
    int LastColumn = XDex1 - 1;
    Cell cell_prev = this.Cells.get(LastColumn);
    Cell cell_now = null;
    for (int XDex = XDex0; XDex < XDex1; XDex++) {
      cell_now = this.Cells.get(XDex);
      cell_now.CrossConnect(cell_prev, 0, 0);// 0 is xdim, 0 is west pole
      cell_prev = cell_now;
    }
  }
  /* ********************************************************************************* */
  public Cell GetCell(int XDex, int YDex) {
    Cell cell = this.Cells.get(YDex * this.NumCols + XDex);
    return cell;
  }
  /* ********************************************************************************* */
  public void Initialize_Ramp() {
    Cell cell;
    double Height;
    for (int YDex = 0; YDex < this.NumRows; YDex++) {
      for (int XDex = 0; XDex < this.NumCols; XDex++) {
        cell = this.GetCell(XDex, YDex);
        if (false) {
          Height = (rand.nextDouble() * 0.1) + YDex;// uphill ramp with noise
        } else {
          Height = YDex;// uphill ramp
        }
        cell.DirtHeightNext = cell.DirtHeight = Height;// uphill ramp
//        cell.DirtHeightNext = cell.DirtHeight = this.NumRows - YDex;// downhill ramp
//        cell.WaterHeightNext = cell.WaterHeight = 1.0;
        cell.WaterHeight = 1.0;
        cell.SumInFlow = cell.SumOutFlow = 0.0;
        cell.SumInFlowNext = cell.SumOutFlowNext = 0.0;;
      }
    }
  }
  /* ********************************************************************************* */
  public ArrayList<Cell> Index_Height() {
    int NumCells = this.Cells.size();
    ArrayList<Cell> index = new ArrayList<Cell>();
    Cell cell;
    for (int CellCnt = 0; CellCnt < NumCells; CellCnt++) {
      cell = this.Cells.get(CellCnt);
      index.add(cell);
    }
    Sort_By_Height(index);
    return index;
  }
  /* ********************************************************************************* */
  public static void Sort_By_Height(ArrayList<Cell> cells) {
    Collections.sort(cells, new Comparator<Cell>() {
      @Override public int compare(Cell cell0, Cell cell1) {
//        return Double.compare(cell0.GetHeight(), cell1.GetHeight());// negative if cell0<cell1. ascending
        return Double.compare(cell1.GetHeight(), cell0.GetHeight());// positive if cell0<cell1. descending
      }
    });
  }
  /* ********************************************************************************* */
  public void Get_Total_Depth() {
    int NumCells = this.Cells.size();
    Cell cell;
    double SumWater = 0.0, SumDirt = 0.0, SumSilt = 0.0;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Cells.get(cnt);// Go from highest elevation to lowest and push water downhill.
      SumWater += cell.WaterHeight;
      SumDirt += cell.DirtHeight;
      SumSilt += cell.Silt;
    }
    double AllDirt = SumDirt + SumSilt;
    System.out.println("SumWater:" + SumWater + ", AllDirt:" + AllDirt + ", SumDirt:" + SumDirt + ", SumSilt:" + SumSilt);
  }
  /* ********************************************************************************* */
  public void Faucet(int XCtr, int YCtr, double Quantity) {
    Cell cell;
    int Radius = 2;
    int Diameter = Radius * 2;
    int XStart = Math.max(0, XCtr - Radius);
    int YStart = Math.max(0, YCtr - Radius);
    int XEnd = Math.min(this.NumCols, XCtr + Radius);
    int YEnd = Math.min(this.NumRows, YCtr + Radius);
    int NumCells = Diameter * Diameter;
    Quantity = Quantity / ((double) NumCells);
    for (int YDex = YStart; YDex < YEnd; YDex++) {
      for (int XDex = XStart; XDex < XEnd; XDex++) {
        cell = this.GetCell(XDex, YDex);
        cell.WaterHeight += Quantity;
      }
    }
  }
  /* ********************************************************************************* */
  public void Drain(int XCtr, int YCtr, double Factor) {// Factor is usually 0 
    Cell cell;
    int Radius = 2;
    int XStart = Math.max(0, XCtr - Radius);
    int YStart = Math.max(0, YCtr - Radius);
    int XEnd = Math.min(this.NumCols, XCtr + Radius);
    int YEnd = Math.min(this.NumRows, YCtr + Radius);
    for (int YDex = YStart; YDex < YEnd; YDex++) {
      for (int XDex = XStart; XDex < XEnd; XDex++) {
        cell = this.GetCell(XDex, YDex);
        cell.WaterHeight *= Factor;
        cell.SumInFlow *= Factor;
        cell.Silt *= Factor;
        cell.SiltNext *= Factor;
      }
    }
  }
  /* ********************************************************************************* */
  public void Rain() {
    int NumCells = this.Cells.size();
    Cell cell;
    double RandValue, SumRain = 0.0;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Cells.get(cnt);// Go from highest elevation to lowest and push water downhill.
      RandValue = rand.nextDouble() * 0.1;//0.01;
      SumRain += RandValue;
      cell.WaterHeight += RandValue;
    }
    // to do: evaporate out SumRain equally from all cells
    // but don't drain any cell less than zero 
    // and be sure to deposit out all concentrated silt
  }
  /* ********************************************************************************* */
  public void RunCycle() {
    int NumCells = this.Cells.size();
    Cell cell;
//    this.Rain();
    double Quantity = 1.0;
    this.Faucet(this.NumCols / 2, this.NumRows - 4, Quantity);// add water
//    this.Faucet(this.NumCols / 2, 4, -Quantity);// drain
    this.Drain(this.NumCols / 2, 4, 0.0);// drain
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Cells.get(cnt);// Go from highest elevation to lowest and push water downhill.
      cell.Push_Water();
    }
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Cells.get(cnt);
      cell.Erode_Deposit();
    }
    for (int cnt = 0; cnt < NumCells; cnt++) {// now update from future values
      cell = this.Cells.get(cnt);
      cell.Rollover();
    }
//    System.out.println("RunCycle done");
  }
  /* ********************************************************************************* */
  public void Run() {
    int NumGens = 5;//1000;
    NumGens = 1000;
    this.Get_Total_Depth();
    for (int gencnt = 0; gencnt < NumGens; gencnt++) {
      this.RunCycle();
    }
    System.out.println("RunCycle_Local done");
    this.Get_Total_Depth();
  }
  /* ********************************************************************************* */
  public static PrintWriter Open_File(String FileName) {
    PrintWriter out = null;
    try {
      out = new PrintWriter(FileName);
    } catch (Exception ex) {
      System.out.println(ex.getStackTrace());
    }
    return out;
  }
  /* ******************************************************************************* */
  public interface CellVal {
    public double GetVal(Cell cell);
  }
  /* ******************************************************************************* */
  public static String ObjPnt(double XLoc, double YLoc, double ZLoc) {
    //String txtln = String.format("v %.16f %.16f %.16f", XLoc, ZLoc, YLoc);
    String txtln = String.format("v %.16f %.16f %.16f", XLoc, YLoc, ZLoc);
    return txtln;
  }
  /* ******************************************************************************* */
  public static String ObjFace(int V0, int V1, int V2, int V3) {
    String txtln = String.format("f %d %d %d %d", V0, V1, V2, V3);//f 1 5 7 3
    return txtln;
  }
  /* ******************************************************************************* */
  public void Grid2Obj(CellVal cellval, String FileName) {
    int GridWdt = this.NumCols, GridHgt = this.NumRows;
    int XDexNext, YDexNext;//https://evermotion.org/tutorials/show/10344/scattering-trees-in-blender-tip-of-the-week
    double ZLoc;
    double Unit = 1.0;
    Cell cell;
    PrintWriter out = Open_File(FileName);
    out.println("o Elevations");
    // First write vertices
    for (YDexNext = 0; YDexNext < GridHgt; YDexNext++) {
      for (XDexNext = 0; XDexNext < GridWdt; XDexNext++) {
        cell = this.GetCell(XDexNext, YDexNext);
        ZLoc = cellval.GetVal(cell);
        String txtln = ObjPnt(XDexNext * Unit, YDexNext * Unit, ZLoc);
        out.println(txtln);
      }
    }

    out.println("s off");

    int V0, V1, V2, V3;// Write faces
    int XDexPrev = 0, YDexPrev = 0;
    for (YDexNext = 1; YDexNext < GridHgt; YDexNext++) {
      XDexPrev = 0;
      for (XDexNext = 1; XDexNext < GridWdt; XDexNext++) {
        V0 = 1 + (YDexPrev * GridWdt) + XDexPrev;// xprev yprev
        V1 = 1 + (YDexPrev * GridWdt) + XDexNext;// xnow yprev
        V2 = 1 + (YDexNext * GridWdt) + XDexNext;// xnow ynow
        V3 = 1 + (YDexNext * GridWdt) + XDexPrev;// xprev ynow
        String txtln = ObjFace(V0, V1, V2, V3);
        out.println(txtln);
        XDexPrev = XDexNext;
      }
      YDexPrev = YDexNext;
    }

    out.close();
    System.out.println();
  }
}
