package erosion;

/**
 *
 * @author MultiTool
 */
public class Cell {
  public static final int NDims = 2;
  public static final int NPoles = 2;
  public static final int NumNbrs = NDims * NPoles;
  public static final double GravAccel = 1.0;
  public static final double FlowRate = 0.1;
  public static final double Whatever = 0.5;
  public static final double Sediment_Capacity_Const = 0.1;//Whatever;// arbitrary values
  public static final double Dissolve_Const = Whatever, Deposit_Const = Whatever;// arbitrary values
//  public static final double Sediment_Capacity_Const = 0.1;// arbitrary values
//  public static final double Dissolve_Const = 0.1, Deposit_Const = 0.1;// arbitrary values

  public int MyIndex = -10000;
  public Link[] Nbrs;
  public double DirtHeight, WaterHeight;
  public double DirtHeightNext;//, WaterHeightNext;
  public double Silt = 0.0, SiltNext;
  public double SumInFlow = 0, SumOutFlow = 0;
  public double SumInFlowNext = 0, SumOutFlowNext = 0;
  public double RecordSpeed = 0.0;
  /* ********************************************************************************* */
  public class Link {// link from nbr to nbr
    public Cell Parent = Cell.this, Child = null;
    public Link Opposite = null;
    public double OutFlow = 0.0;
  }
  /* ********************************************************************************* */
  public Cell() {
    this.Init_NbrCross();
  }
  /* ********************************************************************************* */
  public void Init_NbrCross() {
    this.Nbrs = new Link[NumNbrs];
    for (int NbrCnt = 0; NbrCnt < NumNbrs; NbrCnt++) {
      this.Nbrs[NbrCnt] = null;
    }
  }
  /* ********************************************************************************* */
  public void CrossConnect(Cell other, int Dim, int Pole) {
    Link MyLink, YourLink;
    int DimOffset = Dim * NPoles;

    int Dex = DimOffset + Pole;

    {
      MyLink = this.new Link();
      this.Nbrs[Dex] = MyLink;
      MyLink.Child = other;
    }

    Pole = (1 - Pole);// 1-1 = 0.  1-0 = 1.
    Dex = DimOffset + Pole;

    {
      YourLink = other.new Link();
      other.Nbrs[Dex] = YourLink;
      YourLink.Child = this;// connect along same dimension, but opposite pole (eg your south connects to my north).
    }

    MyLink.Opposite = YourLink;
    YourLink.Opposite = MyLink;
  }
  /* ********************************************************************************* */
  public double GetHeight() {
    return this.DirtHeight + this.WaterHeight;
  }
  /* ********************************************************************************* */
  public double GetSpeed() {// only good after Push_Water() is called
    double Value = (Math.abs(this.SumInFlow) + Math.abs(this.SumOutFlow)) / 2.0;
    //double Value = ((this.SumInFlow) + (this.SumOutFlow)) / 2.0;
//    return Math.min(Math.abs(this.SumInFlow), Math.abs(this.SumOutFlow));
    if (Value < 0) {
      System.out.println();
    }
    return Value;
  }
  /* ********************************************************************************* */
  public void AddInFlow(double WaterAmount, double SiltAmount) {
    this.SumInFlowNext += WaterAmount;
    this.AddInSilt(SiltAmount);
  }
  /* ********************************************************************************* */
  public void AddOutFlow(double WaterAmount, double SiltAmount) {
    this.SumOutFlowNext += WaterAmount;
    this.AddInSilt(-SiltAmount);
  }
//  /* ********************************************************************************* */
//  public void AddInFlow(double WaterAmount) {
//    this.SumInFlow += WaterAmount;
//  }
//  /* ********************************************************************************* */
//  public void AddOutFlow(double WaterAmount) {
//    this.SumOutFlow += WaterAmount;
//  }
  /* ********************************************************************************* */
  public void AddInSilt(double SiltAmount) {
    double before = this.SiltNext;
    this.SiltNext += SiltAmount;
    if (0 < before && this.SiltNext < 0) {
      System.out.println();
    }
  }
  /* ********************************************************************************* */
  public void Rollover() {
    if (this.DirtHeight != this.DirtHeightNext) {
//      System.out.println();
    }
    this.DirtHeight = this.DirtHeightNext;
    this.WaterHeight += this.SumInFlowNext;//this.WaterHeight = this.WaterHeightNext;
    this.WaterHeight -= this.SumOutFlowNext;
    this.Silt = this.SiltNext;
    this.SumInFlow = SumInFlowNext;
    this.SumOutFlow = SumOutFlowNext;
    this.SumInFlowNext = 0.0;
    this.SumOutFlowNext = 0.0;
    this.ResetLinks();
  }
  /* ********************************************************************************* */
  public void ResetLinks() {
    for (int NbrCnt = 0; NbrCnt < NumNbrs; NbrCnt++) {
      this.Nbrs[NbrCnt].OutFlow = 0.0;
    }
  }
  /* ********************************************************************************* */
  public void Push_Water() {
    Cell nbr;
    Link link;
    this.ResetLinks();
    double MyHeight = this.GetHeight();
    double MyWater = this.WaterHeight;
    double MyDirt = this.DirtHeight;
    double MySilt = this.Silt;
    double SiltMoving, SiltRatio;
    double NbrHgt = 0, HgtDiff, SumOutFlow = 0.0;
    double OneFlow;
    double DeltaTime = 1.0, PipeArea = 1.0, PipeLength = 1.0;

    if (MyWater > 0.0) {
      SiltRatio = MySilt / MyWater;
    } else {
      SiltRatio = 0.0;
    }
    if (SiltRatio > 1.0) {
      System.out.print("");
    }
    for (int NbrCnt = 0; NbrCnt < NumNbrs; NbrCnt++) {
      link = this.Nbrs[NbrCnt];
      nbr = link.Child;
      NbrHgt = nbr.GetHeight();
//      HgtDiff = NbrHgt - MyHeight;
      HgtDiff = MyHeight - NbrHgt;
      if (HgtDiff > 0.0) {// only push downhill
        // here we really want to figure out the amount of outflow per nbr
        // probably based on inertia as well as fall distance
        OneFlow = DeltaTime * PipeArea * ((GravAccel * HgtDiff) / PipeLength);
        OneFlow *= FlowRate;
        link.OutFlow = OneFlow;
        SumOutFlow += OneFlow;
      } else {
        link.OutFlow = 0.0;
      }
    }

    if (SumOutFlow > MyWater) {// we used up all our water!
      double fract = MyWater / SumOutFlow;
      for (int NbrCnt = 0; NbrCnt < NumNbrs; NbrCnt++) {
        link = this.Nbrs[NbrCnt];
        link.OutFlow *= fract;
      }
      SumOutFlow = MyWater;
    }

    SiltMoving = SiltRatio * SumOutFlow;
    this.AddOutFlow(SumOutFlow, SiltMoving);

    // push the water to neighbors
    for (int NbrCnt = 0; NbrCnt < NumNbrs; NbrCnt++) {
      link = this.Nbrs[NbrCnt];
      nbr = link.Child;
      OneFlow = link.OutFlow;
      if (OneFlow > 0.0) {// only downhills were computed
        SiltMoving = SiltRatio * OneFlow;
        nbr.AddInFlow(OneFlow, SiltMoving);
      }
    }
    this.RecordSpeed = this.GetSpeed();
  }
  /* ********************************************************************************* */
  public double GetTiltSine() {
    Cell nbr;// The research paper implied this was based on sine of tilt. This departs from that.
    double CellWdtSquared = 1.0;
    double SumHeights = 0.0, AvgHeight, SumDiffs = 0.0, AvgDiff;
//    SumHeights += this.GetHeight();
    for (int nbrcnt = 0; nbrcnt < NumNbrs; nbrcnt++) {
      nbr = this.Nbrs[nbrcnt].Child;
      SumHeights += nbr.GetHeight();
    }
//    AvgHeight = SumHeights / ((double) NumNbrs + 1.0);
    AvgHeight = SumHeights / ((double) NumNbrs);
    for (int nbrcnt = 0; nbrcnt < NumNbrs; nbrcnt++) {
      nbr = this.Nbrs[nbrcnt].Child;
      SumDiffs += Math.abs(nbr.GetHeight() - AvgHeight);
    }
    AvgDiff = SumDiffs / ((double) NumNbrs);
    double Hypot = Math.sqrt((AvgDiff * AvgDiff) + CellWdtSquared);
    double Sine = AvgDiff / Hypot;
    return Sine;
  }
  /* ********************************************************************************* */
  public double GetSedimentCapacity() {
    double Sediment_Capacity = 0.0;
    double FlowSpeed = this.GetSpeed();
    double TiltHeight = this.GetTiltSine();
    //Sediment_Capacity = Sediment_Capacity_Constant * Math.sin(tilt_angle) * FlowSpeed;// from FastErosion pdf
    //Sediment_Capacity = Math.max(slope, minSlope) * FlowSpeed * water_amount * Kq;// from ranmantaru
//    Sediment_Capacity = Sediment_Capacity_Const * TiltHeight * FlowSpeed;// improv
    Sediment_Capacity = Sediment_Capacity_Const * FlowSpeed;// improv
    if (Sediment_Capacity > 1.0){
      System.out.println();
    }
    if (Sediment_Capacity < 0) {
      System.out.println();
    }
    //System.out.println("s:"+ Sediment_Capacity);
    return Sediment_Capacity;
  }
  /* ********************************************************************************* */
  public void Erode_Deposit() {
    double SiltCapacity = this.GetSedimentCapacity();
    double Amount;
    if (this.Silt < SiltCapacity) {// erode
      Amount = Dissolve_Const * (SiltCapacity - this.Silt);
      this.DirtHeightNext = this.DirtHeight - Amount;//b next = bt − Ks(C − st) 
      this.AddInSilt(Amount);//s next = st + Ks(C − st)
    } else if (this.Silt > SiltCapacity) {// deposit
      Amount = Deposit_Const * (this.Silt - SiltCapacity);
      this.DirtHeightNext = this.DirtHeight + Amount;//b next = bt + Kd(st − C) 
      this.AddInSilt(-Amount);//s next = st − Kd(st − C) 
    } else {// equal, do nothing?
    }
  }
  /* ********************************************************************************* */
  public void SiltTransport() {
    /*
     This has to be easy.
     We know how much silt my water volume holds.
     We know what fraction of my water will flow to each nbr.
     SiltToNbr = (fraction of my water sent to nbr) * mysilt/mywater;
     nbr.SiltNext += SiltToNbr;
     just like
     nbr.WaterNext += WaterToNbr;
     */
  }
  /* ********************************************************************************* */
  public Cell Clone_Me() {
    Cell child = new Cell();
    return child;
  }
}
/*

 Next for silt transport

 1. WaterIncrement
 2. FlowSimulation
 3. ErosionDeposition
 4. SedimentTransport
 5. Evaporation

 */
