package erosion;

/**
 *
 * @author MultiTool
 */
public class Erosion {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {

    Grid grid = new Grid();
    grid.Run();

    //Grid.CellVal dirt = (Cell cell) -> cell.DirtHeight;
    Grid.CellVal dirt = new Grid.CellVal() {
      @Override public double GetVal(Cell cell) {
        return cell.DirtHeight;
      }
    };
    grid.Grid2Obj(dirt, "Dirt.obj");

    Grid.CellVal water = new Grid.CellVal() {
      @Override public double GetVal(Cell cell) {
        return cell.WaterHeight;
      }
    };
    grid.Grid2Obj(water, "Water.obj");

    Grid.CellVal silt = new Grid.CellVal() {
      @Override public double GetVal(Cell cell) {
        return cell.Silt;
      }
    };
    grid.Grid2Obj(silt, "Silt.obj");

    Grid.CellVal sumheight = new Grid.CellVal() {
      @Override public double GetVal(Cell cell) {
        return cell.GetHeight();
      }
    };
    grid.Grid2Obj(sumheight, "Height.obj");

    Grid.CellVal speed = new Grid.CellVal() {
      @Override public double GetVal(Cell cell) {
        return cell.RecordSpeed;
      }
    };
    grid.Grid2Obj(speed, "Speed.obj");

    System.out.println("Done");
  }
}
/*
erosion from 
Fast Hydraulic Erosion Simulation and Visualization on GPU
FastErosion_PG07.pdf
-----------------------
depth_1(x, y) = depth_t(x, y) + delta_t * rain_t(x, y)
-----------------------
fLeft_t(x, y) next = //+ delta_t = 
max(0, 
  fLeft_t(x, y) + 
  delta_t * Area *
  ((g * delta_heightLeft(x, y))
  /
  length)
)
-----------------------
deltaheightLeft_t(x, y) = 
bdirt_t(x, y)+depth_1(x, y) - bdirt_t(x-1, y)-depth_1(x-1, y)
-----------------------
K = min(1,
  depth_1 * lengthX*lengthY
  /
  (  (fluxLeft + fluxR + fluxT + fluxB) * delta_t  )
)
-----------------------
fi
t+delta_t(x, y) = K · fi
t+delta_t(x, y), i = L,R, T,B

outflow_next_Left = K * outflow_next_Left;
-----------------------
delta_Volume(x, y) = delta_t · (sum_flux_in -sum_flux_out)
-----------------------
depth_2(x, y) = depth_1(x, y) +
(
  delta_Volume(x, y)
  /
  (lengthX*lengthY)
)
-----------------------
The average amount of water that passes through cell (x, y) per unit time in the X direction

delta_WXdirection =
fluxR(x - 1, y) - fluxL(x, y) + fluxR(x, y) - fluxL(x + 1, y)
/
2
-----------------------
The sediment transport capacity C for the water flow in the cell (x, y) is
calculated as:
C(x, y) = Kc * sin(tilt_angle(x, y)) * abs(velocity(x, y))

Kc is sediment capacity constant

-----------------------
 */
